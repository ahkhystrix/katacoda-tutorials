Perform macro-analysis again, to ensure that our optimization helped.

Reset pg_stat_statements stats:

```sql
select pg_stat_statements_reset();
```{{copy T1}}

> Wait ~30-60 seconds before proceeding – we want to collect some stats.

Check the "Top-5 by `total_time`" again:

```sql
select * from pg_stat_statements order by total_time desc limit 5;
```{{copy T1}}

How do you interpret results? What could go wrong with our optimization?
