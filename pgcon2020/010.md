Try working with thin clones using Database Lab.

> Note, that this step uses pre-configured Postgres.ai Platform API token. It's expiration date is 2020-06-01, so it won't work after that date.

Exit from psql:

```
\q
```{{copy T1}}

Check the list of available snapshots:

```bash
dblab snapshot list
```{{copy T1}}

And see currently active clones:

```bash
dblab clone list
```{{copy T1}}

Create your own clone:

```bash
dblab clone create --username demo111 --password demo111
```{{copy T1}}

If needed, see the Database Lab Client CLI reference: https://postgres.ai/docs/database-lab/6_cli_reference.

Katacoda wouldn't allow working with ports 6000-6100 that Database Lab uses. So, use CloudBeaver to connect to the clone: https://demo.cloudbeaver.io/. Use the following credentials:

- host: `35.223.247.20`
- port: [what `dblab clone create` has just returned]
- username: `demo111`
- password: `demo111`
- database: `test_small` (it's ~ 7 GiB, the are more databases on that Postgres server, total size is ~150 GiB)
